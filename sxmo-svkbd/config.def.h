static const Bool wmborder = True;
static int fontsize = 16;
static const char *fonts[] = {
	"monospace:size=18"
};
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#A83A47", "#0f0f0f"  },
	[SchemePress] = { "#ffffff", "#AC484E" },
	[SchemeHighlight] = { "#ffffff", "#AC484E" },
};
