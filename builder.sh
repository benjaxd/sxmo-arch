#!/bin/sh
for tar in $(find -name '*.tar.*'); do rm $tar; done
for folder in $(ls -d */); do cd $folder ; makepkg -s ; cd ..; done
mkdir -p out
for pkg in $(find -name '*.tar.xz'); do mv $pkg out; done  
