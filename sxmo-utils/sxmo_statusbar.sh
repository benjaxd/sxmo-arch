#!/bin/sh
# include common definitions
# shellcheck source = scripts/core/sxmo_common.sh
. "$(dirname "$0")/sxmo_common.sh"
trap "update" USR1
# disabled for being able to run this shit without vim being killed everytime xd
# pgrep -f sxmo_statusbar.sh | grep -v $$ | xargs -r kill -9
update(){
	# In-call.. show length of call | only if sxmo_modemcall.sh is running
	CALLINFO=" "
	if pgrep -f sxmo_modemcall.sh; then
		NOWS="$(date +"%s")"
		CALLSTARTS="$(date +"%s" -d "$(
			grep -aE 'call_start|call_pickup' "$XDG_DATA_HOME"/sxmo/modem/modemlog.tsv |
				tail -n1 |
				cut -f1
		)")"
		CALLSECONDS="$(echo "$NOWS" - "$CALLSTARTS" | bc)"
		CALLINFO=" ${CALLSECONDS}s "
	fi
	# W Symbol if wireless is connect
	WIRELESS=""
	WLANSTATE="$(tr -d "\n" < /sys/class/net/wlp1s0/operstate)" # i changed wlan0 with wlp1s0 for NS
	[ "$WLANSTATE" = "up" ] && WIRELESS="W "

	# M symbol if modem monitoring is on & modem present
	MODEMMON=""
	pgrep -f sxmo_modemmonitor.sh && MODEMMON="M "
	
	# Battery percentage
	BATPATH=/sys/class/power_supply/*bat*/ # slightly modified path, should still match PinePhone
	PCT="$(cat $BATPATH/capacity)" 
	BATSTATUS="$( cat $BATPATH/status | cut -c1)"
	
	# Volume
#	AUDIODEV="$(sxmo_audiocurrentdevice.sh)" # it doesn't get mine kek | might need some rework
	## kill me suckless philosophy but i'm using PA
	
	VOL="$(pactl list sinks |
	       	grep '^[[:space:]]Volumen:' | 
		\head -n $(( $SINK + 1 )) |
	       	tail -n 1 | 
		sed -e 's,.* \([0-9][0-9]*\)%.*,\1,'
	)"
	# Time
	TIME="$(date +%R)"

	BAR="${CALLINFO}${MODEMMON}${WIRELESS}${VOL} ${BATSTATUS}${PCT}% ${TIME}"
	xsetroot -name "$BAR"
}

	# E.g on first boot just to make sure the bar comes in quickly
update && sleep 1 && update && sleep 1
while :
do
	update
	sleep 30 && wait
done

	
